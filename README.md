# README #

Author: Diego Martins de Queiroz.


WebService Rest criado usando Jax-Rs, Jersey, Jetty, Hibernate, Spring and H2 Database.

### Running Tests ###

mvn test

### Starting Application ###

mvn exec:java

Se��es criadas com os nomes a1,a2,a3,a4,a5

### Exemplos ###

* Consultar volume total por tipo de bebida 
**http://localhost:8080/estoque/consultarVolumeTotalTipoBebida/NAO_ALCOOLICA

* Receber volume 
**http://localhost:8080/estoque/receber/NAO_ALCOOLICA/50/a1/diego

* Liberar volume 
**http://localhost:8080/liberar/receber/ALCOOLICA/50/a1/diego

* Consultar locais disponiveis para volume e tipo bebida **http://localhost:8080/estoque/consultarLocaisDisponiveisParaVolumeBebida/NAO_ALCOOLICA/50

* Consultar locais disponiveis para venda 
**http://localhost:8080/estoque/consultarLocaisDisponiveisParaVenda/NAO_ALCOOLICA

* Consultar historico registros consultarHistorico 
**http://localhost:8080/estoque/consultarHistorico/NAO_ALCOOLICA/a1/ENTRADA
http://localhost:8080/estoque/consultarHistorico/NAO_ALCOOLICA/a1/ENTRADA?sort=nome