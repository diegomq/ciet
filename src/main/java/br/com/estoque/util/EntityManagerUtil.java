package br.com.estoque.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtil {

	private static EntityManager entityManager;
	
	public EntityManagerUtil() {
		
	}
	
	public static EntityManager getEntityManager(){
		if(entityManager == null){
			EntityManagerFactory factory = Persistence.createEntityManagerFactory("EstoqueTest");
			entityManager =  factory.createEntityManager();			
		}
		
		return entityManager;
	}

}
