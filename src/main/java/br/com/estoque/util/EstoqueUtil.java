package br.com.estoque.util;

import java.util.Arrays;

import br.com.estoque.DAO.EstoqueDAO;
import br.com.estoque.entity.Estoque;
import br.com.estoque.entity.Secao;

public class EstoqueUtil {
	
	private Estoque estoque;
	
	private static EstoqueUtil estoqueUtil = new EstoqueUtil();
	private EstoqueUtil() {
	}
	
	
	public Estoque getEstoque(EstoqueDAO estoqueDAO) {
		if(this.estoque == null) {
			Secao secao1 = new Secao("a1");
			Secao secao2 = new Secao("a2");
			Secao secao3 = new Secao("a3");
			Secao secao4 = new Secao("a4");
			Secao secao5 = new Secao("a5");
			
			estoque = new Estoque(Arrays.asList(secao1,secao2,secao3,secao4,secao5) );
			estoque = estoqueDAO.save(estoque);
		}
		return this.estoque;
	}


	public static EstoqueUtil getInstance() {
		return estoqueUtil;
	}

}
