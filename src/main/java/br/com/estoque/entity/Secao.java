package br.com.estoque.entity;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import br.com.estoque.entity.RegistroHistorico.TIPO_REGISTRO;

@Entity
@Table(name="secao")
public class Secao extends Entidade{

	private static final long serialVersionUID = 6826892821594148389L;
	
	@NotNull(message = "Nome da se��o n�o pode ser nulo.")
	@Size(min=1 , message = "Nome da se��o n�o pode ser vazio.")
	@Column(name = "nome")
	private String nome;
	
	@OneToMany(fetch= FetchType.LAZY, mappedBy="secao", cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<RegistroHistorico> registros;

	public Secao() {
	}

	public Secao(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RegistroHistorico> getRegistros() {
		return registros;
	}

	public void setRegistros(List<RegistroHistorico> registros) {
		this.registros = registros;
	}

	/**
	 * bebida alcoolica pode ate 500l e nao alcoolica ate 400l
	 * @param tipoBebida
	 * @param volume
	 * @return
	 */
	public boolean podeReceber(TIPO_BEBIDA tipoBebida, Integer volume) {

		if(registros!= null && !registros.isEmpty()) {
			List<RegistroHistorico> registrosValidos = getRegistrosValidos();
			
			TIPO_BEBIDA tipoBebidaAtual = registrosValidos.get(0).getTipoBebida();
			Date dataUltimoRegistro = registrosValidos.stream().max(Comparator.comparing(RegistroHistorico::getData)).get().getData();
			
			int volumeDisponivel = getVolumeDisponivel(registrosValidos);
			if(TIPO_BEBIDA.ALCOOLICA.equals(tipoBebida)) {
				if(volumeDisponivel + volume > 500) {
					return false;
				}
			}else  {
				if(volumeDisponivel + volume > 400) {
					return false;
				}
			}
			
			if(volumeDisponivel > 0) {
				if(!tipoBebida.equals(tipoBebidaAtual))
					return false;
			}else if(volumeDisponivel == 0 && tipoBebidaAtual.equals(TIPO_BEBIDA.ALCOOLICA) && tipoBebida.equals(TIPO_BEBIDA.NAO_ALCOOLICA) ) {
				LocalDateTime ultimRegistro = LocalDateTime.ofInstant(dataUltimoRegistro.toInstant(), ZoneId.systemDefault());
				LocalDateTime hoje = LocalDateTime.now();
				
				if(ChronoUnit.DAYS.between(ultimRegistro, hoje) == 0l)
					return false;
				
			}
			
			return true;
		}else {
			return true;			
		}
	}
	
	public boolean possuiTipoBebidaEVolume(TIPO_BEBIDA tipoBebida, Integer volume) {
		if(registros!= null && !registros.isEmpty()) {
			List<RegistroHistorico> registrosValidos = getRegistrosValidos();
			
			TIPO_BEBIDA tipoBebidaAtual = registrosValidos.get(0).getTipoBebida();
			if(!tipoBebidaAtual.equals(tipoBebida))
				return false;
			
			int volumeDisponivel = getVolumeDisponivel(registrosValidos);
			if(volumeDisponivel < volume)
				return false;
			
			
			return true;
		}
		return false;
	}

	public int getVolumeDisponivel(TIPO_BEBIDA tipoBebida) {
		List<RegistroHistorico> registrosValidos = getRegistrosValidos();
		if(!registrosValidos.isEmpty()) {
			TIPO_BEBIDA tipoBebidaAtual = registrosValidos.get(0).getTipoBebida();
			if(tipoBebidaAtual.equals(tipoBebida))
				return getVolumeDisponivel(registrosValidos);
		}
		return 0;
	}
	
	public int getVolumeDisponivel(List<RegistroHistorico> registrosValidos) {
		int volumeTotalEntrada = registrosValidos.stream().filter(r -> r.getTipoRegistro().equals(TIPO_REGISTRO.ENTRADA)).mapToInt(r -> r.getVolume()).sum();
		int volumeTotalSaida = registrosValidos.stream().filter(r -> r.getTipoRegistro().equals(TIPO_REGISTRO.SAIDA)).mapToInt(r -> r.getVolume()).sum();
		int volumeDisponivel = volumeTotalEntrada - volumeTotalSaida;
		return volumeDisponivel;
	}

	/**
	 *  retorna todos apenas os registros do que contem na secao
	 * @return
	 */
	private List<RegistroHistorico> getRegistrosValidos() {
		//ordenar registros por data desc
		List<RegistroHistorico> registroValidos = new ArrayList<>();
		if(registros != null) {
			registros.sort(Comparator.comparing(RegistroHistorico::getData).reversed());
			TIPO_BEBIDA tipoBebidaAtual = null;
			for (RegistroHistorico registroHistorico : registros) {
				if(tipoBebidaAtual == null) {
					tipoBebidaAtual = registroHistorico.getTipoBebida();
				}
				
				if(!registroHistorico.getTipoBebida().equals(tipoBebidaAtual)) {
					break;
				}
				
				registroValidos.add(registroHistorico);
				
			}
		}
		return registroValidos;
	}

	public void adicionar(RegistroHistorico registroHistorico) {
		if(this.registros == null)
			this.registros = new ArrayList<>();
		
		this.registros.add(registroHistorico);
		
	}



}
