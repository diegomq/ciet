package br.com.estoque.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="pessoa")
public class Pessoa extends Entidade {

	private static final long serialVersionUID = -5699583803102896690L;
	
	@NotNull(message = "Nome da pessoa n�o pode ser nulo.")
	@Size(min=1 , message = "Nome da pessoa n�o pode ser vazio.")
	@Column(name = "nome")
	private String nome;
	
	public Pessoa() {
	}

	public Pessoa(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
