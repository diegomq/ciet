package br.com.estoque.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "bebida")
public class Bebida extends Entidade{

	private static final long serialVersionUID = -1233315102154834552L;
	
	@NotNull(message = "Nome da bebida n�o pode ser nulo.")
	@Size(min=1 , message = "Nome da bebida n�o pode ser vazio.")
	private String nome;
	
	@NotNull(message = "Tipo da bebida n�o pode ser nulo.")	
	private TIPO_BEBIDA tipo;

	public enum TIPO_BEBIDA {
		ALCOOLICA, NAO_ALCOOLICA;

		public static TIPO_BEBIDA obterTipo(String tipoBebida) {
			try {
				return TIPO_BEBIDA.valueOf(tipoBebida);
			}catch(Exception e) {}
			return null;
		}
	};
	
	public Bebida(String nome, TIPO_BEBIDA tipo ) {
		this.nome = nome;
		this.tipo = tipo;
	}

	public Bebida() {
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TIPO_BEBIDA getTipo() {
		return tipo;
	}

	public void setTipo(TIPO_BEBIDA tipo) {
		this.tipo = tipo;
	}

}
