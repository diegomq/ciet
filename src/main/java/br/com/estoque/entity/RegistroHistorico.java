package br.com.estoque.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;

@Entity
@Table(name="registro_historico")
public class RegistroHistorico extends Entidade{

	private static final long serialVersionUID = -8292962229939008913L;

	public enum TIPO_REGISTRO{
		ENTRADA,SAIDA;

		public static TIPO_REGISTRO obterTipo(String tipoRegistro) {
			try {
				return TIPO_REGISTRO.valueOf(tipoRegistro);
			}catch(Exception e) {
				return null;
			}
		}
	}

	@NotNull(message = "Data do registro � obrigat�rio")
	@Column(name = "data")
	private Date data;
	
	@NotNull(message = "Tipo da bebida � obrigat�rio")
	@Column(name = "tipo_bebida")
	@Enumerated(EnumType.STRING)
	private TIPO_BEBIDA tipoBebida;
	
	@NotNull(message = "Tipo do registro � obrigat�rio")
	@Column(name = "tipo_registro")
	@Enumerated(EnumType.STRING)
	private TIPO_REGISTRO tipoRegistro;
	
	@NotNull(message = "Volume do registro � obrigat�rio")
	@Column(name = "volume")
	private Integer volume;
	
	@NotNull(message = "Se��o de armazenamento � obrigat�rio")
	@JoinColumn(name = "secao")
	@ManyToOne(fetch=FetchType.LAZY)
	private Secao secao;
	
	@NotNull(message = "Respons�vel pela a��o � obrigat�rio")
	@JoinColumn(name = "responsavel")
	@ManyToOne(fetch=FetchType.LAZY, cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private Pessoa responsavel;
	
	

	public RegistroHistorico() {
	}

	public RegistroHistorico(TIPO_BEBIDA tipoBebida, Integer volume, Pessoa responsavel, Secao secao, TIPO_REGISTRO tipoRegistro) {
		this.tipoBebida = tipoBebida;
		this.volume = volume;
		this.responsavel = responsavel;
		this.secao = secao;
		this.tipoRegistro = tipoRegistro;
		this.data = new Date();
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public TIPO_BEBIDA getTipoBebida() {
		return tipoBebida;
	}

	public void setTipoBebida(TIPO_BEBIDA tipo) {
		this.tipoBebida = tipo;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Secao getSecao() {
		return secao;
	}

	public void setSecao(Secao secao) {
		this.secao = secao;
	}

	public Pessoa getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Pessoa responsavel) {
		this.responsavel = responsavel;
	}

	public TIPO_REGISTRO getTipoRegistro() {
		return this.tipoRegistro;
	}

	public void setTipoRegistro(TIPO_REGISTRO tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

}
