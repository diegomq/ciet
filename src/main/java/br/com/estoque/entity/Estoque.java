package br.com.estoque.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import br.com.estoque.entity.RegistroHistorico.TIPO_REGISTRO;

@Entity
@Table(name="estoque")
public class Estoque extends Entidade {
	
	private static final long serialVersionUID = 4645205697390908809L;
	
	@NotNull(message=" Obrigat�rio informar as secoes do estoque.")
	@Size(min = 1, message=" Obrigat�rio informar as secoes do estoque.")
	@OneToMany(fetch=FetchType.LAZY,  cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	private List<Secao> secoes;
	
	public Estoque() {
	}
	
	public void receberBebida(TIPO_BEBIDA tipoBebida, Integer volume, Pessoa pessoa, String secao) {
		if(this.secoes == null)
			this.secoes = new ArrayList<Secao>();
		
		Optional<Secao> encontrouSecao = secoes.stream().filter(s -> s.podeReceber(tipoBebida,volume) && s.getNome().equalsIgnoreCase(secao)).findFirst();
		if(encontrouSecao.isPresent()) {
			Secao secaoEncontrada = encontrouSecao.get();
			RegistroHistorico registroHistorico = new RegistroHistorico(tipoBebida, volume, pessoa, secaoEncontrada, TIPO_REGISTRO.ENTRADA);
			secaoEncontrada.adicionar(registroHistorico);
		}else {
			throw new RuntimeException("Secao n�o pode receber bebida");
		}
	}
	
	public void liberarBebida(TIPO_BEBIDA tipoBebida, Integer volume, Pessoa responsavel, String secao) {
		if(this.secoes == null)
			this.secoes = new ArrayList<Secao>();
		
		Optional<Secao> encontrouSecao = secoes.stream().filter(s -> s.possuiTipoBebidaEVolume(tipoBebida,volume) && s.getNome().equals(secao)).findFirst();
		if(encontrouSecao.isPresent()) {
			Secao secaoEncontrada = encontrouSecao.get();
			RegistroHistorico registroHistorico = new RegistroHistorico(tipoBebida, volume, responsavel, secaoEncontrada, TIPO_REGISTRO.SAIDA);
			secaoEncontrada.adicionar(registroHistorico);
		}else {
			throw new RuntimeException("N�o h� secao com a bebida pedida");
		}
	}
	
	public Integer obterVolumeTotalTipoBebida(TIPO_BEBIDA tipoBebida) {
		if(this.secoes == null) {
			return 0;
		}
		return secoes.stream().mapToInt(s -> s.getVolumeDisponivel(tipoBebida)).sum();
	}
	
	public List<Secao> obterSecoesDisponiveis(TIPO_BEBIDA tipo, Integer volume) {
		if(this.secoes != null && !this.secoes.isEmpty()) {
			return this.secoes.stream().filter(s -> s.podeReceber(tipo, volume)).collect(Collectors.toList());
		}
		return null;
	}
	
	public List<Secao> obterSecoesPossuem(TIPO_BEBIDA tipoBebida) {
		if(this.secoes != null && !this.secoes.isEmpty()) {
			return this.secoes.stream().filter(s -> s.getVolumeDisponivel(tipoBebida) > 0).collect(Collectors.toList());
		}
		return null;
	}

	public Estoque(List<Secao> secoes) {
		this.secoes = secoes;
	}

	public List<Secao> getSecoes() {
		return secoes;
	}

	public void setSecoes(List<Secao> secoes) {
		this.secoes = secoes;
	}

	
	
}
