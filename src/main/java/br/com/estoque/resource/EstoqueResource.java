package br.com.estoque.resource;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.estoque.DAO.EstoqueDAO;
import br.com.estoque.DAO.PessoaDAO;
import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import br.com.estoque.entity.Estoque;
import br.com.estoque.entity.Pessoa;
import br.com.estoque.entity.RegistroHistorico.TIPO_REGISTRO;
import br.com.estoque.entity.Secao;
import br.com.estoque.util.EstoqueUtil;

@Component
@Path("/estoque")
public class EstoqueResource {
	
	@Autowired
	private EstoqueDAO estoqueDAO;
	
	@Autowired
	private PessoaDAO pessoaDAO;
	
	@GET
	@Path("/consultarVolumeTotalTipoBebida/{tipoBebida}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String consultarVolumeTotalTipoBebida(@PathParam("tipoBebida") String tipoBebida) {
		Estoque estoque = EstoqueUtil.getInstance().getEstoque(estoqueDAO);
		TIPO_BEBIDA tipo = TIPO_BEBIDA.obterTipo(tipoBebida);
		if(tipo != null) {
			Integer volumeTotal = estoque.obterVolumeTotalTipoBebida(tipo);
			return volumeTotal.toString();
		}
		return "Tipo de bebida n�o encontrada. Informe ALCOOLICA OU NAO_ALCOOLICA. ";
	}
	
	@GET
	@Path("/receber/{tipoBebida}/{volume}/{secao}/{pessoa}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String receber(@PathParam("tipoBebida") String tipoBebida,@PathParam("volume") Integer volume, @PathParam("secao") String secao, @PathParam("pessoa") String nomePessoa) {
		Estoque estoque = EstoqueUtil.getInstance().getEstoque(estoqueDAO);
		TIPO_BEBIDA tipo = TIPO_BEBIDA.obterTipo(tipoBebida);
		try {
			if(tipo != null) {
				
				Pessoa pessoa = pessoaDAO.pesquisarPor(nomePessoa);
				if(pessoa == null)
					pessoa = new Pessoa(nomePessoa);
				estoque.receberBebida(tipo, volume, pessoa, secao);
				estoque = estoqueDAO.alterar(estoque);
				return "Bebida armazenada com sucesso!";
				
			}
			return "Tipo de bebida n�o encontrada. Informe ALCOOLICA OU NAO_ALCOOLICA. ";
		}catch(Exception e) {
			return e.getMessage();
		}
	}
	
	@GET
	@Path("/liberar/{tipoBebida}/{volume}/{secao}/{pessoa}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String liberar(@PathParam("tipoBebida") String tipoBebida,@PathParam("volume") Integer volume, @PathParam("secao") String secao, @PathParam("pessoa") String nomePessoa) {
		Estoque estoque = EstoqueUtil.getInstance().getEstoque(estoqueDAO);
		TIPO_BEBIDA tipo = TIPO_BEBIDA.obterTipo(tipoBebida);
		try {
			if(tipo != null) {
				
				Pessoa pessoa = pessoaDAO.pesquisarPor(nomePessoa);
				if(pessoa == null)
					pessoa = new Pessoa(nomePessoa);
				estoque.liberarBebida(tipo, volume, pessoa, secao);
				estoque = estoqueDAO.alterar(estoque);
				return "Bebida liberada com sucesso!";
				
			}
			return "Tipo de bebida n�o encontrada. Informe ALCOOLICA OU NAO_ALCOOLICA. ";
		}catch(Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	
	
	
	@GET
	@Path("/consultarLocaisDisponiveisParaVolumeBebida/{tipoBebida}/{volume}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String consultarLocaisDisponiveisParaVolumeBebida(@PathParam("tipoBebida") String tipoBebida, @PathParam("volume") Integer volume) {
		TIPO_BEBIDA tipo = TIPO_BEBIDA.obterTipo(tipoBebida);
		if(tipo != null) {
			List<Secao> secoes = EstoqueUtil.getInstance().getEstoque(estoqueDAO).obterSecoesDisponiveis(tipo,volume);
			if(secoes != null && !secoes.isEmpty()) {
				String opcoes = secoes.stream().map(s -> s.getNome()).collect(Collectors.joining(","));
				return "As se��es "+ opcoes +" est�o dispon�veis.";
			}
			return "N�o se��es disponiveis para o tipo de bebida e volume informado";
		}
		return "Tipo de bebida n�o encontrada. Informe ALCOOLICA OU NAO_ALCOOLICA. ";
	}
	
	@GET
	@Path("/consultarLocaisDisponiveisParaVenda/{tipoBebida}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String consultarLocaisDisponiveisParaVenda(@PathParam("tipoBebida") String tipoBebida) {
		TIPO_BEBIDA tipo = TIPO_BEBIDA.obterTipo(tipoBebida);
		if(tipo != null) {
			List<Secao> secoes =  EstoqueUtil.getInstance().getEstoque(estoqueDAO).obterSecoesPossuem(tipo);
			
			if(secoes != null && !secoes.isEmpty()) {
				String opcoes = secoes.stream().map(s -> s.getNome()).collect(Collectors.joining(","));
				return "As se��es "+ opcoes +" est�o dispon�veis para venda.";
			}
			return "N�o h� se��es com o tipo de bebida.";
			
		}
		return "Tipo de bebida n�o encontrada. Informe ALCOOLICA OU NAO_ALCOOLICA. ";
	}
	
	@GET
	@Path("/consultarHistorico/{tipoBebida}/{secao}/{tipoRegistro}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Object consultaHistorico(@PathParam("tipoBebida") String tipoBebida, @PathParam("secao") String secao, @PathParam("tipoRegistro") String tipoRegistro, @QueryParam("sort")List<String> ordenacao) {
		TIPO_BEBIDA tipo = TIPO_BEBIDA.obterTipo(tipoBebida);
		TIPO_REGISTRO tipoReg = TIPO_REGISTRO.obterTipo(tipoRegistro);
		if(tipo == null) {
			return "Tipo de bebida n�o encontrada. Informe ALCOOLICA OU NAO_ALCOOLICA. ";
		}
		if(tipoReg == null) {
			return "Tipo de registro n�o encontrada. Informe ENTRADA OU SAIDA. ";
		}
		
		return estoqueDAO.pesquisarRegistros(tipo, secao, tipoReg, ordenacao);
	}


}
