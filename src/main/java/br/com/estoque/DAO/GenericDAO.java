package br.com.estoque.DAO;

import javax.persistence.EntityManager;

import br.com.estoque.repository.GenericRepository;
import br.com.estoque.util.EntityManagerUtil;

public abstract class GenericDAO<T> implements GenericRepository<T>{
	
	public T save(T t) {
		EntityManager em = EntityManagerUtil.getEntityManager();
		em.getTransaction().begin();
		t = em.merge(t);
		em.getTransaction().commit();
		return t;
		
	}

	public T alterar(T t) {
		EntityManager em = EntityManagerUtil.getEntityManager();
		em.getTransaction().begin();
		em.merge(t);
		em.getTransaction().commit();	
		return t;
	}
	
}
