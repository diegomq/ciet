package br.com.estoque.DAO;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import br.com.estoque.entity.Pessoa;
import br.com.estoque.repository.PessoaRepository;
import br.com.estoque.util.EntityManagerUtil;

@Component
public class PessoaDAO extends GenericDAO<Pessoa>implements PessoaRepository {

	@SuppressWarnings("unchecked")
	@Override
	public Pessoa pesquisarPor(String nome) {
		StringBuilder sql = new StringBuilder();
		sql.append("Select p from Pessoa p ");
		sql.append(" where p.nome =:nome ");
		
		Query createQuery = EntityManagerUtil.getEntityManager().createQuery(sql.toString());
		createQuery.setParameter("nome", nome);
		createQuery.setMaxResults(1);
		
		List<Pessoa> resultList = createQuery.getResultList();
		if(resultList.isEmpty())
			return null;
		return resultList.get(0);
	}

}
