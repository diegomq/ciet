package br.com.estoque.DAO;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.springframework.stereotype.Component;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import br.com.estoque.entity.Estoque;
import br.com.estoque.entity.RegistroHistorico;
import br.com.estoque.entity.RegistroHistorico.TIPO_REGISTRO;
import br.com.estoque.repository.EstoqueRepository;
import br.com.estoque.util.EntityManagerUtil;

@Component
public class EstoqueDAO  extends GenericDAO<Estoque> implements EstoqueRepository  {

	@SuppressWarnings("unchecked")
	public List<RegistroHistorico> pesquisarRegistros(TIPO_BEBIDA tipo, String secao, TIPO_REGISTRO tipoReg, List<String> ordenacao) {
		StringBuilder sql = new StringBuilder();
		sql.append(" Select rh from RegistroHistorico rh ");
		sql.append(" join fetch rh.secao s  ");
		sql.append(" WHERE  rh.tipoRegistro = :tipoReg ");
		sql.append(" AND rh.tipoBebida = :tipoBebida ");
		sql.append(" AND s.nome = :secao ");
		
		if(ordenacao == null || ordenacao.isEmpty())
			sql.append("order by rh.data, s.nome ");
		else {
			sql.append(" order by ");
			sql.append(ordenacao.stream().collect(Collectors.joining(",")));
		}
		
		Query createQuery = EntityManagerUtil.getEntityManager().createQuery(sql.toString());
		createQuery.setParameter("tipoReg", tipoReg);
		createQuery.setParameter("tipoBebida", tipo);
		createQuery.setParameter("secao", secao);
		List<RegistroHistorico> registros = createQuery.getResultList();
		return registros;
	}

	

}
