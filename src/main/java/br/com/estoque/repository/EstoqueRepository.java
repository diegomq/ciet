package br.com.estoque.repository;

import br.com.estoque.entity.Estoque;

public interface EstoqueRepository extends GenericRepository<Estoque> {

}
