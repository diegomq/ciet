package br.com.estoque.repository;

public interface GenericRepository<T> {

	
	public T save(T t);
	public T alterar(T t);
	
}
