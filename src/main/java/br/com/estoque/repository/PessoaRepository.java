package br.com.estoque.repository;

import br.com.estoque.entity.Pessoa;

public interface PessoaRepository {

	public Pessoa pesquisarPor(String nome);
}
