package br.com.estoque.entity;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import br.com.estoque.entity.RegistroHistorico.TIPO_REGISTRO;
import junit.framework.TestCase;

public class RegistroHistoricoTest extends GenericTest{
	
	@Test
	public void testeInvalidaRegistroSeAoMenosUmDosCamposForemNulos() {
		RegistroHistorico registroHistorico = new RegistroHistorico();
		Set<ConstraintViolation<RegistroHistorico>> erros = validator.validate(registroHistorico);
		TestCase.assertTrue(erros.size() == 6);
	}
	
	@Test
	public void testaValidaRegistroSeTodosCamposForemInformados() {
		Pessoa responsavel = new Pessoa("Diego");
		Integer volume = 50;
		Secao secao = new Secao("A1");
		RegistroHistorico registroHistorico = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, volume, responsavel, secao, TIPO_REGISTRO.ENTRADA);
		Set<ConstraintViolation<RegistroHistorico>> erros = validator.validate(registroHistorico);
		TestCase.assertTrue(erros.size() == 0);
	}

}
