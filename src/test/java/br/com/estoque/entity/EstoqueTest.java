package br.com.estoque.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import junit.framework.TestCase;


public class EstoqueTest extends GenericTest{
	
	
	@Test
	public void testaInvalidaEstoqueComSecoesNulas() {
		Estoque estoque = new Estoque();
		Set<ConstraintViolation<Estoque>> listaErros = validator.validate(estoque);
		TestCase.assertTrue(listaErros.size()>0);
		TestCase.assertTrue(listaErros.stream().anyMatch( le -> le.getMessage().contains("Obrigatório informar as secoes do estoque.")));
	}
	
	@Test
	public void testaInvalidaEstoqueComSecoesVazias() {
		Estoque estoque = new Estoque(new ArrayList<>());
		Set<ConstraintViolation<Estoque>> listaErros = validator.validate(estoque);
		TestCase.assertTrue(listaErros.size()>0);
		TestCase.assertTrue(listaErros.stream().anyMatch( le -> le.getMessage().contains("Obrigatório informar as secoes do estoque.")));
	}
	
	
	@Test
	public void testaValidaEstoqueComSecoesVazias() {
		Estoque estoque = new Estoque(Arrays.asList(new Secao()));
		Set<ConstraintViolation<Estoque>> listaErros = validator.validate(estoque);
		TestCase.assertFalse(listaErros.size()>0);
		TestCase.assertFalse(listaErros.stream().anyMatch( le -> le.getMessage().contains("Obrigatório informar as secoes do estoque.")));
	}
	
	@Test
	public void testaRetornoVolumeSecoesTipoBebida() {
		Estoque estoque = getEstoque();
		estoque.receberBebida(TIPO_BEBIDA.ALCOOLICA, 50, new Pessoa("Diego"), "a1");
		
		Integer volumeEsperado = 50;
		Integer volumeEsperadoNaoAlcoolica = 0;
		Integer volumeEncontrado = estoque.obterVolumeTotalTipoBebida(TIPO_BEBIDA.ALCOOLICA);
		Integer volumeEncontradoNaoAlcoolica = estoque.obterVolumeTotalTipoBebida(TIPO_BEBIDA.NAO_ALCOOLICA);
		TestCase.assertEquals(volumeEsperado, volumeEncontrado);
		TestCase.assertEquals(volumeEsperadoNaoAlcoolica, volumeEncontradoNaoAlcoolica);
	}
	
	
	@Test
	public void testeLiberarBebida() {
		Estoque estoque = getEstoque();
		estoque.receberBebida(TIPO_BEBIDA.ALCOOLICA, 50, new Pessoa("Diego"), "a1");
		estoque.liberarBebida(TIPO_BEBIDA.ALCOOLICA, 20, new Pessoa("Diego"), "a1");
		
		List<Secao> obterSecoesDisponiveis = estoque.obterSecoesDisponiveis(TIPO_BEBIDA.ALCOOLICA, 30);
		TestCase.assertTrue(obterSecoesDisponiveis.stream().anyMatch(s -> s.getNome().equalsIgnoreCase("a1")));
		
		TestCase.assertFalse(obterSecoesDisponiveis.stream().anyMatch(s -> s.getNome().equalsIgnoreCase("a2")));
	}

	private Estoque getEstoque() {
		Secao secao1 = new Secao("a1");
		Secao secao2 = new Secao("b1");
		Secao secao3 = new Secao("c1");
		Secao secao4 = new Secao("d1");
		Secao secao5 = new Secao("e1");
		Estoque estoque = new Estoque(Arrays.asList(secao1,secao2,secao3, secao4, secao5));
		return estoque;
	}
	

}
