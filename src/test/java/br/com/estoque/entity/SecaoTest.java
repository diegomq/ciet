package br.com.estoque.entity;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import br.com.estoque.entity.Bebida.TIPO_BEBIDA;
import br.com.estoque.entity.RegistroHistorico.TIPO_REGISTRO;
import junit.framework.TestCase;

public class SecaoTest extends GenericTest{

	
	@Test
	public void testeInvalidaSecaoComNomeNulo() {
		Secao secao = new Secao();
		Set<ConstraintViolation<Secao>> erros = validator.validate(secao);
		TestCase.assertTrue(erros.size()>0);
		TestCase.assertTrue(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da se��o n�o pode ser nulo")));
	}
	
	@Test
	public void testeInvalidaSecaoComNomeVazio() {
		Secao secao = new Secao("");
		Set<ConstraintViolation<Secao>> erros = validator.validate(secao);
		TestCase.assertTrue(erros.size()>0);
		TestCase.assertTrue(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da se��o n�o pode ser vazio")));
	}
	
	@Test
	public void testeValidaSecaoComNomePreenchido() {
		Secao secao = new Secao("A1");
		Set<ConstraintViolation<Secao>> erros = validator.validate(secao);
		TestCase.assertFalse(erros.size()>0);
		TestCase.assertFalse(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da se��o n�o pode ser nulo")));
		TestCase.assertFalse(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da se��o n�o pode ser vazio")));
	}
	
	@Test
	public void testaAceitaTipoBebidaQdoHaEspacoSecao() {
		Secao secao = new Secao("a1");
		TIPO_BEBIDA tipo = TIPO_BEBIDA.ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertTrue(aceitouBebida);
	}
	
	/**
	 * Uma se��o n�o pode ter dois ou mais tipos diferentes de bebidas (como j� fora dito) 
	 */
	@Test
	public void testaSecaoNaoPodeTerMaisDeUmTipoDeBebida() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		secao.getRegistros().add(registroHistoricoOldEntrada);
		
		TIPO_BEBIDA tipo = TIPO_BEBIDA.ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertFalse(aceitouBebida);
	}
	/**
	 * Uma se��o n�o pode receber bebidas n�o alco�licas se recebeu alco�licas no mesmo dia. 
	 */
	@Test
	public void testaSecaoNaoPodeReceberBebidaNaoAlcoolicaSeRecebeuAlcoolicaNoMesmoDia() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		LocalDateTime data5DiasAtras = LocalDateTime.now().minusDays(5);
		LocalDateTime dataHoje = LocalDateTime.now();
				
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		registroHistoricoOldEntrada.setData(Date.from(data5DiasAtras.atZone(ZoneId.systemDefault()).toInstant()));
		
		RegistroHistorico registroHistoricoOldSaida = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		registroHistoricoOldSaida.setData(Date.from(dataHoje.atZone(ZoneId.systemDefault()).toInstant()));
		
		secao.getRegistros().add(registroHistoricoOldEntrada);
		secao.getRegistros().add(registroHistoricoOldSaida);
		
		TIPO_BEBIDA tipo = TIPO_BEBIDA.NAO_ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertFalse(aceitouBebida);
	}
	
	/**
	 * Uma se��o n�o pode receber bebidas n�o alco�licas se recebeu alco�licas no mesmo dia. 
	 */
	@Test
	public void testaSecaoPodeReceberBebidaNaoAlcoolicaSeRecebeuAlcoolicaNoDiaAnterior() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		LocalDateTime data5DiasAtras = LocalDateTime.now().minusDays(5);
		LocalDateTime dataHoje = LocalDateTime.now().minusDays(1);
				
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		registroHistoricoOldEntrada.setData(Date.from(data5DiasAtras.atZone(ZoneId.systemDefault()).toInstant()));
		
		RegistroHistorico registroHistoricoOldSaida = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		registroHistoricoOldSaida.setData(Date.from(dataHoje.atZone(ZoneId.systemDefault()).toInstant()));
		
		secao.getRegistros().add(registroHistoricoOldEntrada);
		secao.getRegistros().add(registroHistoricoOldSaida);
		
		TIPO_BEBIDA tipo = TIPO_BEBIDA.NAO_ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertTrue(aceitouBebida);
	}
	
	
	/**
	 * Uma se��o n�o pode receber bebidas n�o alco�licas se recebeu alco�licas no mesmo dia. 
	 */
	@Test
	public void testaSecaoPodeReceberBebidaAlcoolicaSeRecebeuNaoAlcoolicaNoMesmoDia() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		LocalDateTime data5DiasAtras = LocalDateTime.now().minusDays(5);
		LocalDateTime dataOntem = LocalDateTime.now().minusDays(1);
				
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		registroHistoricoOldEntrada.setData(Date.from(data5DiasAtras.atZone(ZoneId.systemDefault()).toInstant()));
		
		RegistroHistorico registroHistoricoOldSaida = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 400, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		registroHistoricoOldSaida.setData(Date.from(dataOntem.atZone(ZoneId.systemDefault()).toInstant()));
		
		secao.getRegistros().add(registroHistoricoOldEntrada);
		secao.getRegistros().add(registroHistoricoOldSaida);
		
		TIPO_BEBIDA tipo = TIPO_BEBIDA.ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertTrue(aceitouBebida);
	}
	
	@Test
	public void testaNaoAceitaTipoBebidaQdoSecaoNaoTemEspacoSuficiente() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 480, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		RegistroHistorico registroHistoricoOldSaida = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 480, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		RegistroHistorico registroHistorico = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 480, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		RegistroHistorico registroHistoricoSaida = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 20, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		secao.getRegistros().add(registroHistorico);
		secao.getRegistros().add(registroHistoricoSaida);
		secao.getRegistros().add(registroHistoricoOldEntrada);
		secao.getRegistros().add(registroHistoricoOldSaida);
		
		
		TIPO_BEBIDA tipo = TIPO_BEBIDA.ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertFalse(aceitouBebida);
	}
	
	
	@Test
	public void testaNaoAceitaTipoBebidaQdoSecaoNaoTemEspacoSuficienteNaoAlcoolica() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		
		LocalDateTime data5DiasAtras = LocalDateTime.now().minusDays(5);
		LocalDateTime dataOntem = LocalDateTime.now().minusDays(1);
		
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 380, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		registroHistoricoOldEntrada.setData(Date.from(data5DiasAtras.atZone(ZoneId.systemDefault()).toInstant()));
		RegistroHistorico registroHistoricoOldSaida = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 380, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		registroHistoricoOldSaida.setData(Date.from(dataOntem.atZone(ZoneId.systemDefault()).toInstant()));
		
		RegistroHistorico registroHistorico = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 380, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		RegistroHistorico registroHistoricoSaida = new RegistroHistorico(TIPO_BEBIDA.NAO_ALCOOLICA, 20, new Pessoa("Diego"), secao, TIPO_REGISTRO.SAIDA);
		secao.getRegistros().add(registroHistorico);
		secao.getRegistros().add(registroHistoricoSaida);
		secao.getRegistros().add(registroHistoricoOldEntrada);
		secao.getRegistros().add(registroHistoricoOldSaida);
		
		
		TIPO_BEBIDA tipo = TIPO_BEBIDA.NAO_ALCOOLICA;
		int volume = 50;
		
		boolean aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertFalse(aceitouBebida);
		
		secao.getRegistros().add(registroHistoricoSaida);
		secao.getRegistros().add(registroHistoricoSaida);
		
		aceitouBebida = secao.podeReceber(tipo, volume);
		TestCase.assertTrue(aceitouBebida);
	}
	
	@Test
	public void testaSeSecaoPossuiTipoBebidaEVolumePedido() {
		Secao secao = new Secao("a1");
		secao.setRegistros(new ArrayList<>());
		RegistroHistorico registroHistoricoOldEntrada = new RegistroHistorico(TIPO_BEBIDA.ALCOOLICA, 380, new Pessoa("Diego"), secao, TIPO_REGISTRO.ENTRADA);
		secao.adicionar(registroHistoricoOldEntrada);
		
		boolean possuiBebidaAlcoolicaVolume30 = secao.possuiTipoBebidaEVolume(TIPO_BEBIDA.ALCOOLICA, 30);
		boolean naoPossuiBebidaAlcoolicaParaVolume390 = secao.possuiTipoBebidaEVolume(TIPO_BEBIDA.ALCOOLICA, 390);
		boolean naoPossuiBebidaNaoAlcoolica = secao.possuiTipoBebidaEVolume(TIPO_BEBIDA.NAO_ALCOOLICA, 390);
		
		TestCase.assertTrue(possuiBebidaAlcoolicaVolume30);
		TestCase.assertFalse(naoPossuiBebidaAlcoolicaParaVolume390);
		TestCase.assertFalse(naoPossuiBebidaNaoAlcoolica);
	}
}
