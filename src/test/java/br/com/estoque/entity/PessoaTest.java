package br.com.estoque.entity;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.junit.Test;

import junit.framework.TestCase;

public class PessoaTest extends GenericTest {

	
	@Test
	public void testeInvalidaPessoaComNomeNulo() {
		Pessoa pessoa = new Pessoa();
		Set<ConstraintViolation<Pessoa>> erros = validator.validate(pessoa);
		TestCase.assertTrue(erros.size()>0);
		TestCase.assertTrue(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da pessoa n�o pode ser nulo")));
	}
	
	@Test
	public void testeInvalidaPessoaComNomeVazio() {
		Pessoa pessoa = new Pessoa("");
		Set<ConstraintViolation<Pessoa>> erros = validator.validate(pessoa);
		TestCase.assertTrue(erros.size()>0);
		TestCase.assertTrue(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da pessoa n�o pode ser vazio")));
	}
	
	@Test
	public void testeValidaPessoaComNomePreenchido() {
		Secao secao = new Secao("A1");
		Set<ConstraintViolation<Secao>> erros = validator.validate(secao);
		TestCase.assertFalse(erros.size()>0);
		TestCase.assertFalse(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da pessoa n�o pode ser nulo")));
		TestCase.assertFalse(erros.stream().anyMatch(e -> e.getMessage().contains("Nome da se��o n�o pode ser vazio")));
	}
}
